using System;

namespace Crnc.Oms.Notification.Push.Client.Auth
{
    public class AuthInfoDto
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}