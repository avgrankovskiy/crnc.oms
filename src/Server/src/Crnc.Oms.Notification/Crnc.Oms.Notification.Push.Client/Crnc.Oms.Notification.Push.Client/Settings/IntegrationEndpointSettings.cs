﻿namespace Crnc.Oms.Notification.Gateway.Integration.Settings
{
    public class IntegrationEndpointSettings
    {
        public string PushNotificationServiceEndpoint { get; set; }
        public string SecurityServiceEndpoint { get; set; }
    }
}