namespace Crnc.Oms.Notification.Gateway.Integration.Settings
{
    public class AuthSettings
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}