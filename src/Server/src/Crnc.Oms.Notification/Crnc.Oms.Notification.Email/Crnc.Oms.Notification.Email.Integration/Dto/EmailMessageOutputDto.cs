﻿using System;

namespace Crnc.Oms.Notification.Gateway.Integration.Dto
{
    public class EmailMessageOutputDto
    {
        public Guid MessageId { get; set; }
    }
}