﻿namespace Crnc.Oms.Notification.Email.Integration.Settings
{
    public class IntegrationEndpointSettings
    {
        public string MessageBrokerEndpoint { get; set; }
    }
}