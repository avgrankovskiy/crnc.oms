﻿using System;

namespace Crnc.Oms.Notification.Email.Application.Dto
{
    public class SendEmailMessageOutputDto
    {
        public Guid MessageId { get; set; }
    }
}