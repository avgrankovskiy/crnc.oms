﻿using System;

namespace Crnc.Oms.Notification.Gateway.Application.Dto
{
    public class SendNotificationToUserOutputDto
    {
        public Guid MessageId { get; set; }
    }
}