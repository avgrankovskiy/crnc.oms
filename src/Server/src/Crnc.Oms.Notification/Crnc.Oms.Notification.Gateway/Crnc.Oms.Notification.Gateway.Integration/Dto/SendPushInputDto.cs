﻿using System;
using System.ComponentModel.DataAnnotations;
using Crnc.Oms.Messaging.Contract.Commands;

namespace Crnc.Oms.Notification.Gateway.Integration.Dto
{
    public class SendPushInputDto
        : SendPushNotificationToReceiverCommand
    { 
        public Guid MessageId { get; set; }

        public Guid ReceiverUserId { get; set; }

        public string Message { get; set; }

        public SendPushInputDto(Guid messageId, Guid receiverUserId, string message)
        {
            MessageId = messageId;
            ReceiverUserId = receiverUserId;
            Message = message;
        }
    }
}