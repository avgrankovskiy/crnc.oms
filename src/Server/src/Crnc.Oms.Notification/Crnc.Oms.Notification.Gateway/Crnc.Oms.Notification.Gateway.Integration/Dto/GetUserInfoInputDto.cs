﻿using System;

namespace Crnc.Oms.Notification.Email.Integration.Dto
{
    public class GetUserInfoInputDto
    {
        public Guid UserId { get; set; }
    }
}