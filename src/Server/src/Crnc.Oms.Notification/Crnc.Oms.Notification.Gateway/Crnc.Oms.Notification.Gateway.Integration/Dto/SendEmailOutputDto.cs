﻿using System;

namespace Crnc.Oms.Notification.Gateway.Integration.Dto
{
    public class SendEmailOutputDto
    {
        public Guid MessageId { get; set; }
    }
}