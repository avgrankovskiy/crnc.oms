﻿using System;

namespace Crnc.Oms.Notification.Gateway.Integration.Dto
{
    public class SendPushOutputDto
    {
        public Guid MessageId { get; set; }
    }
}