﻿using System;

namespace Crnc.Oms.Notification.Push.Application.Dto
{
    public class SendPushMessageOutputDto
    {
        public Guid MessageId { get; set; }
    }
}