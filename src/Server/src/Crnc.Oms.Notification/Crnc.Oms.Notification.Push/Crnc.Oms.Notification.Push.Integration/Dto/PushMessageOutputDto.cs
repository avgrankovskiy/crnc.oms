﻿using System;

namespace Crnc.Oms.Notification.Push.Integration.Dto
{
    public class PushMessageOutputDto
    {
        public Guid MessageId { get; set; }
    }
}