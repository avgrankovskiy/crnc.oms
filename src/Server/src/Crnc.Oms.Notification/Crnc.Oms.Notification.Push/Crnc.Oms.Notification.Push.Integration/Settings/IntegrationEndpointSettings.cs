﻿namespace Crnc.Oms.Notification.Push.Integration.Settings
{
    public class IntegrationEndpointSettings
    {
        public string UiEndpoint { get; set; }
        public string MessageBrokerEndpoint { get; set; }
    }
}