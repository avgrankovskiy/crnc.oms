namespace Crnc.Oms.Security.Infrastructure.DataAccess
{
    public class MongoDbSettings
    {
        public string Database { get; set; }
        public string Server { get; set; }
    }
}