namespace Crnc.Oms.Security.WebApi.Authorization
{
    public class Roles
    {
        public const string Admin = "Admin";

        public const string MainManager = "Main manager";

        public const string Manager = "Manager";
    }
}