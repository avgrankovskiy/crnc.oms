﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Crnc.Oms.Security.Domain.Repositories;
using Crnc.Oms.Security.Domain.Aggregates.Users;
using UserEntity = Crnc.Oms.Security.Domain.Aggregates.Users.User;
using System.ComponentModel.DataAnnotations;
using Crnc.Oms.Security.Domain.Dto;
using Crnc.Oms.Security.Domain.Validation;
using Crnc.Oms.Security.Infrastructure.CrossCutting;
using Crnc.Oms.Security.Infrastructure.DataAccess.Exceptions;
using Crnc.Oms.Security.WebApi.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NSwag.Annotations;

namespace Crnc.Oms.Security.WebApi.Controllers
{
    /// <summary>
    /// Management of users 
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize]
    public class UsersController 
        : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Get all users or by filter
        /// </summary>
        /// <remarks>Requires admin role</remarks>
        /// <response code="200">Returns users.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [AllowAnonymous]
        public async Task<ActionResult> Get([FromQuery]UserFilterDto dto)
        {
            if (dto.IsShortInfo)
            {
                var users = await _userRepository.FindByFilterShortInfoAsync(dto);

                return Ok(users);
            }
            else
            {
                var users = await _userRepository.FindByFilterAsync(dto);

                return Ok(users);
            }
        }

        /// <summary>
        /// Get users by Id
        /// </summary>
        /// <param name="id">Id of user, for example: <example>2a89985f-f013-4f2a-9545-395efb43a142</example></param>
        /// <remarks>Requires admin role</remarks>
        /// <response code="200">Returns users.</response>
        /// <response code="404">Not found user.</response>
        [HttpGet("{id:Guid}")]
        [ProducesResponseType(typeof(UserItemDto),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var user = await _userRepository.FindByIdAsync(id);

                return Ok(new UserItemDto()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    FullName = user.FullName,
                    Email = user.Email,
                    Password = user.PasswordHash,
                    Login = user.Login,
                    Phone = user.Phone,
                    Role = user.Role.Title,
                    RoleId = user.Role.Id,
                    PhotoBase64 = user.Photo?.ContentBase64,
                    PhotoMimeType = user.Photo?.MimeType,
                    IsActive = user.IsActive
                });
            }
            catch (MissingEntityException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <remarks>Requires admin role</remarks>
        /// <response code="200">User has created</response>
        /// <response code="400">User is not valid.</response>
        [HttpPost]
        [Authorize(Roles = Roles.Admin)]
        [OpenApiOperation("Create user", "Create new user", "Requires admin role")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody]SaveUserDto user)
        {
            if (ModelState.IsValid)
            {
                var password = PasswordHelper.GetHash(user.Password);

                UserPhoto photo = null;
                if (!string.IsNullOrWhiteSpace(user.PhotoBase64)
                    && !string.IsNullOrWhiteSpace(user.PhotoMimeType))
                    photo = new UserPhoto(user.PhotoBase64, user.PhotoMimeType);

                var role = await _userRepository.GetRoleByIdAsync(user.RoleId);
                
                var entity = UserEntity.CreateNew(user.Login, password.Hash, password.Salt,
                    user.FirstName, user.LastName, user.Email, user.Phone, role, photo);

                try
                {
                    await _userRepository.AddAsync(entity);
                }
                catch (EntityAlreadyExistedException e)
                {
                    return BadRequest(e.Message);
                }

                return Ok(entity.Id);
            }
            else
                return BadRequest(ModelState);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <remarks>Requires admin role</remarks>
        /// <response code="200">User has updated</response>
        /// <response code="400">User is not valid.</response>
        /// <response code="404">User has not found.</response>
        [HttpPut("{id:Guid}")]
        [Authorize(Roles = Roles.Admin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Put(Guid id, [FromBody]SaveUserDto user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var password = PasswordHelper.GetHash(user.Password);
                    
                    UserPhoto photo = null;
                    if (!string.IsNullOrWhiteSpace(user.PhotoBase64)
                        && !string.IsNullOrWhiteSpace(user.PhotoMimeType))
                        photo = new UserPhoto(user.PhotoBase64, user.PhotoMimeType);

                    var role = await _userRepository.GetRoleByIdAsync(user.RoleId);
                    
                    var entity = UserEntity.CreateExisted(id, user.Login, password.Hash, password.Salt,
                        user.FirstName, user.LastName, user.Email, role, 
                        user.IsActive, user.Phone, photo);

                    await _userRepository.SaveAsync(entity);

                    return Ok();
                }
                catch (MissingEntityException)
                {
                    return NotFound();
                }
            }
            
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <remarks>Requires admin role</remarks>
        /// <response code="200">User has deleted</response>
        /// <response code="404">User has not found.</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.Admin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                await _userRepository.DeleteAsync(id);
                
                return Ok();
            }
            catch (MissingEntityException)
            {
                return NotFound();
            }
        }
    }
}
