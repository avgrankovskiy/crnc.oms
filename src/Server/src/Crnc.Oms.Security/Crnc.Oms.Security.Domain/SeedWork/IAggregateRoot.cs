﻿namespace Crnc.Oms.Security.Domain.SeedWork
{
    /// <summary>
    /// Marker of aggregate's root
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
