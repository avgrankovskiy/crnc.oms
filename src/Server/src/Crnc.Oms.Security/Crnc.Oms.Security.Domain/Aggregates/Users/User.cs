﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crnc.Oms.Security.Domain.SeedWork;

namespace Crnc.Oms.Security.Domain.Aggregates.Users
{
    /// <summary>
    /// User of application
    /// </summary>
    [Table("Users")]
    public class User
        : DomainEntity, IAggregateRoot
    {
        #region Constructors and Factories
        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="login">Username/login</param>
        /// <param name="passwordHash">Hash of password</param>
        /// <param name="passwordSalt">Salt of password</param>
        /// <param name="firstName">FirstName</param>
        /// <param name="lastName">LastName</param>
        /// <param name="email">Email</param>
        /// <param name="phone">Phone</param>
        /// <param name="role">Role</param>
        /// <param name="photo">Photo</param>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public static User CreateNew(string login, string passwordHash, string passwordSalt,
            string firstName, string lastName, string email, string phone=null, Role role=null, UserPhoto photo = null, Guid? id = null)
        {
            return new User()
            {
                Id = id ?? Guid.NewGuid(),
                Login = login,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Role = role ?? new Role("Manager"),
                Phone = phone,
                Photo = photo,
                IsActive = true
            };
        }

        public static User CreateExisted(Guid id,string login, string passwordHash, string passwordSalt,
            string firstName, string lastName, string email, Role role, bool isActive,string phone = null, UserPhoto photo = null)
        {
            return new User()
            {
                Id = id,
                Login = login,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Role = role,
                Phone = phone,
                Photo = photo,
                IsActive = isActive
            };               
        }

        private User()
        {

        } 
        #endregion

        #region Properties

        /// <summary>
        /// User name
        /// </summary>
        public string Login { get; private set; }

        /// <summary>
        /// Password hash
        /// </summary>
        public string PasswordHash { get; private set; }


        /// <summary>
        /// Password hash
        /// </summary>
        public string PasswordSalt { get; private set; }

        /// <summary>
        /// Full name
        /// </summary>        
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// First name
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; private set; }

        /// <summary>
        /// Email address
        /// </summary>
        public string Email { get; private set; }
    
        public string Phone { get; private set; }

        /// <summary>
        /// Property of active user
        /// </summary>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Photo of user
        /// </summary>
        public UserPhoto Photo { get; set; }

        /// <summary>
        /// Role
        /// </summary>
        public Role Role { get; private set; }

        #endregion

        #region Behavior

        /// <summary>
        /// Change login
        /// </summary>
        /// <param name="login">login</param>
        public void ChangeLogin(string login)
        {
            if (string.IsNullOrWhiteSpace(login))
                throw new ArgumentNullException("New login is empty");

            Login = login;
        }

        /// <summary>
        /// Change password (hash of password)
        /// </summary>
        /// <param name="passwordHash">password's hash</param>
        /// <param name="passwordSalt">password's salt</param>
        public void ChangePassword(string passwordHash, string passwordSalt)
        {
            if (string.IsNullOrWhiteSpace(passwordHash) || string.IsNullOrWhiteSpace(passwordSalt))
                throw new ArgumentNullException("New password is empty");

            PasswordHash = passwordHash;
            PasswordSalt = passwordSalt;
        }

        /// <summary>
        /// Change photo
        /// </summary>
        /// <param name="photo">photo</param>
        public void ChangePhoto(UserPhoto photo)
        { 
            if(photo != null)
            {
                if (Photo != null)
                {
                    Photo.Content = photo.Content;
                    Photo.MimeType = photo.MimeType;
                }
                else
                    Photo = photo;
            }               
        }

        /// <summary>
        /// Change first name
        /// </summary>
        /// <param name="firstName">new first name</param>
        public void ChangeFirstName(string firstName)
        {
            if (string.IsNullOrWhiteSpace(firstName))
                throw new ArgumentNullException("Changed first name is empty");

            FirstName = firstName;
        }

        /// <summary>
        /// Change last name
        /// </summary>
        /// <param name="lastName">new last name</param>
        public void ChangeLastName(string lastName)
        {
            if (string.IsNullOrWhiteSpace(lastName))
                throw new ArgumentNullException("Changed last name is empty");

            LastName = lastName;
        }

        /// <summary>
        /// Change email
        /// </summary>
        /// <param name="email">email</param>
        public void ChangeEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException("Changed email is empty");

            Email = email;
        }

        /// <summary>
        /// Change phone
        /// </summary>
        /// <param name="phone">phone</param>
        public void ChangePhone(string phone)
        {
            Phone = phone;
        }
        
        /// <summary>
        /// Change role
        /// </summary>
        /// <param name="role">role</param>
        public void ChangeRole(Role role)
        {
            Role = role;
        }

        /// <summary>
        /// Make user disactive
        /// </summary>
        public void Deactivate()
        {
            IsActive = false;
        }

        /// <summary>
        /// Make user active
        /// </summary>
        public void Activate()
        {
            IsActive = true;
        }

        #endregion
    }
}
