﻿using System;

namespace Crnc.Oms.Security.Domain.Dto
{
    public class TextValueDto
    {
        public Guid Value { get; set; }

        public string Text { get; set; }
    }
}
