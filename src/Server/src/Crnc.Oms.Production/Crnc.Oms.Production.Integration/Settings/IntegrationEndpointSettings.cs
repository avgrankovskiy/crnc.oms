﻿﻿namespace Crnc.Oms.Production.Integration.Settings
{
    public class IntegrationEndpointSettings
    {
        public string MessageBrokerEndpoint { get; set; }
    }
}