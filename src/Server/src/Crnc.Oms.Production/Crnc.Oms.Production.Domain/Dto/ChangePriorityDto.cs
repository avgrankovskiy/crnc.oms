﻿using Crnc.Oms.Production.Domain.Aggregates.JobAggregate;

namespace Crnc.Oms.Production.Domain.Dto
{
    public class ChangePriorityDto
    {
        public Priority Priority { get; set; }
    }
}