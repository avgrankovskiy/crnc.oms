﻿namespace Crnc.Oms.Production.Domain.SeedWork
{
    /// <summary>
    /// Marker of aggregate's root
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
