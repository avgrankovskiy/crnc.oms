using Crnc.Oms.Sales.Application.Features.Orders.Dto.Output;

namespace Crnc.Oms.Sales.Application.Features.Orders.Dto.Input
{
    public class GetNewOrderInputDto
        : IUseCaseQuery<GetNewOrderOutputDto>
    {
    }
}