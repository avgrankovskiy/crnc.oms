﻿using System;

namespace Crnc.Oms.Sales.Application.Features.Orders.Dto.Output
{
    public class CreateOrderOutputDto
    {
        public Guid Id { get; set; }
    }
}