using MediatR;

namespace Crnc.Oms.Sales.Application
{
    /// <summary>
    /// Marker interface for data of query for user scenario
    /// </summary>
    /// <typeparam name="TOut">Type of result data</typeparam>
    public interface IUseCaseQuery<TOut>
        : IRequest<TOut>
    {
        
    }
}