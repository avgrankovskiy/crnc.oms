﻿namespace Crnc.Oms.Sales.Domain.Aggregates.Order
{
    /// <summary>
    /// Types of job for estimate
    /// </summary>
    public enum JobType
    {
        New = 1,

        Repair = 2,

        Service = 3,

        Other = 4
    }
}
