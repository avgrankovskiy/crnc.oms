﻿namespace Crnc.Oms.Sales.Domain.SeedWork
{
    /// <summary>
    /// Marker of aggregate's root
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
