using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Crnc.Oms.Messaging.Contract.Commands;
using Crnc.Oms.Messaging.Contract.Events;
using Crnc.Oms.Sales.Domain.SeedWork;
using Crnc.Oms.Sales.Application;
using Crnc.Oms.Sales.Application.Features.Orders.Commands;
using Crnc.Oms.Sales.Application.Features.Orders.Dto;
using Crnc.Oms.Sales.Application.Features.Orders.Dto.Input;
using Crnc.Oms.Sales.Application.Features.Orders.Dto.Output;
using Crnc.Oms.Sales.Application.Features.Orders.Queries;
using Crnc.Oms.Sales.DataAccess;
using Crnc.Oms.Sales.DataAccess.Repositories;
using Crnc.Oms.Sales.Domain.Aggregates.Order;
using Crnc.Oms.Sales.Domain.Gateways;
using Crnc.Oms.Sales.Domain.Repositories;
using Crnc.Oms.Sales.Integration.Dto;
using Crnc.Oms.Sales.Integration.Gateways;
using Crnc.Oms.Sales.Integration.Settings;
using Crnc.Oms.Sales.WebApi.Authorization;
using Crnc.Oms.Sales.WebApi.Consumers;
using Crnc.Oms.Sales.WebApi.Middlewares;
using GreenPipes;
using MassTransit;
using MassTransit.AspNetCoreIntegration;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using NSwag;
using Newtonsoft.Json.Converters;
using Prometheus;

namespace Crnc.Oms.Sales.WebApi
{
    public class Startup
    {
        public ILoggerFactory LoggerFactory { get; }
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();
            
            services.AddCors(options => {
                options.AddPolicy("AllOrigins", builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod());
            });
            
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            services.AddDbContext<SalesDataContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("OmsSalesDb"));
            });
            
            services.Configure<IntegrationEndpointSettings>(Configuration.GetSection("IntegrationEndpoints"));

            IBusControl CreateBus(IServiceProvider serviceProvider) => Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var integrationSettings = new Crnc.Oms.Sales.Integration.Settings.IntegrationEndpointSettings();
                Configuration.GetSection("IntegrationEndpoints").Bind(integrationSettings);
            
                cfg.Host(integrationSettings.MessageBrokerEndpoint);
            
                cfg.ReceiveEndpoint("jobCreatedForOrder", e =>
                {
                    e.ConfigureConsumer<JobCreatedForOrderConsumer>(serviceProvider);
                });

                EndpointConvention.Map<SendNotificationToUserCommand>(new Uri($"{integrationSettings.MessageBrokerEndpoint}/commands/sendNotificationToUser"));
            });
            
            void ConfigureMassTransit(IServiceCollectionConfigurator configurator)
            {
                configurator.AddConsumer<JobCreatedForOrderConsumer>();
            }
            
            services.AddMassTransit(CreateBus, ConfigureMassTransit);

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<IDomainEventDispatcher, DomainEventDispatcher>();
            services.AddMediatR(typeof(IDomainEventNotificationHandler).Assembly);
            
            services.AddScoped<IOrderRepository, OrderRepository>();
            
            services.AddScoped<IEmployeeGateway, EmployeeSecurityGateway>();
            services.AddScoped<INotificationGateway, MessageBrokerNotificationGateway>();
            services.AddScoped<IProductionJobGateway, ProductionJobGateway>();
            services.AddScoped<ICurrentUserContext, CurrentUserContext>();

            services.AddSingleton<ICurrentDateTimeProvider, CurrentDateTimeProvider>();
            services.AddScoped<ICommandQueryDispatcher, CommandQueryDispatcher>();
            
            services.Configure<AuthSettings>(Configuration.GetSection("Auth"));
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    var authSettings = new AuthSettings();
                    Configuration.GetSection("Auth").Bind(authSettings);

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = authSettings.JwtIssuer,
                        ValidAudience = authSettings.JwtAudience,
                        IssuerSigningKey = authSettings.SymmetricSecurityKey
                    };
                });
            
            services.AddOpenApiDocument(options =>
            {
                //Title in header of api
                options.Title = "Crnc Oms Sales API Doc";
                //Version in header of api
                options.Version = "1.0";
                options.AddSecurity("JWT", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Please insert JWT with Bearer into field"
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, SalesDataContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseHealthChecks("/health", new HealthCheckOptions {Predicate = check => check.Tags.Contains("ready")});
            
            var cultureInfo = new CultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            app.UseMonitoringRequestMiddleware();
            
            app.UseRouting();
            app.UseHttpMetrics();
            app.UseCors("AllOrigins");
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(e =>
            {
                e.MapControllers();
                e.MapMetrics();
            });

            app.UseOpenApi();
            app.UseSwaggerUi3();

            SalesDbInitializer.Initialize(dbContext);
        }
    }
}