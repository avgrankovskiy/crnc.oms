﻿namespace Crnc.Oms.Sales.Integration.Dto
{
    public class UserRoles
    {
        public const string Admin = "Admin";

        public const string MainManager = "Main manager";

        public const string Manager = "Manager";
    }
}