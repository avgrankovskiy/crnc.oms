﻿﻿namespace Crnc.Oms.Sales.Integration.Settings
{
    public class IntegrationEndpointSettings
    {
        public string NotificationServiceEndpoint { get; set; }
        public string SecurityServiceEndpoint { get; set; }
        
        public string MessageBrokerEndpoint { get; set; }
    }
}