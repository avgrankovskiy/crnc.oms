import JobsGridRootStore from "./jobsGrid/JobsGridRootStore";


export default interface JobsRootStores{
    jobsGridRootStore?: JobsGridRootStore;
}