export default interface TextValueDto{
    value: number | string;
    text: string;
}
