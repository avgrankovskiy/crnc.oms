import * as React from "react";
import { observer, inject } from "mobx-react";
import NewOrderCardStore from "./NewOrderCardStore";
import { FormProps } from "semantic-ui-react";
import NewOrderModel from "./NewOrderModel";
import OrderCardRootStore from "./OrderCardRootStore";
import OrderCard from "./OrderCard";
import { nameof } from "ts-simple-nameof";
import OrdersRootStores from "../OrdersRootStores";

@inject(nameof<OrdersRootStores>(x => x.orderCardRootStore))
@observer
export default class NewOrderCard extends React.Component<NewOrderCardProps>{

    private readonly store: NewOrderCardStore;

    constructor(props: NewOrderCardProps){
        super(props);

        if(!props.orderCardRootStore)
            throw Error("Not found store");

        this.store = props.orderCardRootStore.newOrderCardStore;

        this.onSave = this.onSave.bind(this);
    }

    async componentDidMount(){
        const {store} = this;
        let newOrder = await store.getNewOrder(); 
        store.setModel(newOrder);
    }

    onSave(event: React.FormEvent<HTMLFormElement>, data: FormProps){
        const {store} = this;

        store.createOrder();           
    }


    public render(){              
        const {onSave, store} = this;

        return (
            <OrderCard
                orderCardRootStore = {store.orderCardRootStore} 
                onSave = {onSave}
                headerText = "Add new order"
            />
        );
    }
}


interface NewOrderCardProps{
    orderCardRootStore?: OrderCardRootStore;
}
