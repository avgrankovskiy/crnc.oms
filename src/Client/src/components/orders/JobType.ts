export enum JobType{
    New = 1,
    Repair = 2,
    Service = 3,
    Other = 4
}